//EXERCICIO 02 - FAÇA UM SANDUÍCHE

hamburguer = 1

alert("Vamos criar um sanduíche com apenas 9 passos!")

switch (hamburguer) {
    case 1: 
        const acao1 = prompt("Passo 1: Escolha o tipo de pão (ex: integral, branca, etc.)")
        alert("O primeiro passo é: " + acao1)
        break;

    case 2: 
        const acao2 = prompt("Passo 2: Escolha o tipo de queijo")
        alert("O segundo passo é: " + acao2)
        break;

    case 3:
        const acao3 = prompt("Passo 3: Escolha a maionese")
        alert("Agora, após escolher o tipo de queijo, é hora de adicionar a maionese. O terceiro passo é: " + acao3)
        break;

    case 4:
        const acao4 = prompt("Passo 4: Escolha o tipo de carne (ex: frango, carne bovina, etc.)")
        alert("Após a maionese, escolha o tipo de carne para o seu sanduíche. O quarto passo é: " + acao4)
        break;
    
    case 5: 
        const acao5 = prompt("Passo 5: Escolha os vegetais (ex: alface, tomate, etc.)")
        alert("Adicione sabor e frescor ao seu sanduíche escolhendo os vegetais desejados. O quinto passo é: " + acao5)
        break;

    case 6: 
        const acao6 = prompt("Passo 6: Adicione molhos e temperos")
        alert("Agora é hora de realçar os sabores com molhos e temperos. O sexto passo é: " + acao6)
        break;

    case 7:
        const acao7 = prompt("Passo 7: Escolha acompanhamentos (ex: batatas fritas, salada, etc.)")
        alert("Para complementar, escolha os acompanhamentos desejados. O sétimo passo é: " + acao7)
        break;

    case 8:
        const acao8 = prompt("Passo 8: Monte o sanduíche")
        alert("Agora, com todos os ingredientes escolhidos, é hora de montar o sanduíche. O oitavo passo é: " + acao8)
        break;

    case 9:
        const acao9 = prompt("Passo 9: Aprecie o seu delicioso sanduíche!")
        alert("Parabéns! Seu sanduíche está pronto para ser saboreado. O nono passo é: " + acao9)
        break;
}