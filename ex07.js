//EXERCICIO 07 - Saber qual salário a receber levando em consideração gratificação e imposto

let salarioBase = parseFloat(prompt('Digite seu salário base'))

let gratificacao = salarioBase * 0.05

let imposto = salarioBase * 0.07

let salarioReceber = salarioBase + gratificacao - imposto

alert(`O salário a receber do funcionário é: R$ ${salarioReceber.toFixed(2)}`);